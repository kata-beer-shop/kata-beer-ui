## Kata Beer UI

## Author

-   Victoria Doe

## Software

-   React v18 - Next 14.0.1
-   Typescript v4.3.5

## Subject

An en E-commerce Interface that allows customers to purchase beers and administrators to manage the store

## How to run the project

To run the project locally, first make sure you have Node and npm installed.  
Clone this [URL]("https://gitlab.com/kata-beer-shop/kata-beer-front.git")

You must first install all the node_modules by executing the following command :

```bash
  npm install
```

To run the project in development mode (watch mode i.e., each change to a file will restart the server and be instantly taken into account) :

```bash
  npm run dev
```

To run the project in production mode

```bash
  npm run build
  npm run start
```

You will also need to set the following environment variables in a .env.development (if project is run in development mode) file at the root of the project (do not wrap variables in quotes):

| Environment Variable | Default Value | Type    |
| -------------------- | ------------- | ------- |
| API_URL              | hidden        | integer |
