/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    env: {
        API_URL: process.env.API_URL
    },
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'images.punkapi.com'
            },
            {
                protocol: 'https',
                hostname: 'tailwindui.com'
            }
        ],
        unoptimized: true
    },
    async redirects() {
        return [
            {
                source: '/',
                destination: '/beer-list',
                permanent: true
            }
        ]
    }
}

module.exports = nextConfig
