import { API_URL } from './shared'
import { Beer } from '../model/Beer'

export const BeerApi = {
    async getAllBeers(): Promise<Array<Beer>> {
        try {
            const response = await fetch(`${API_URL}/beers`)
            return await response.json()
        } catch (error) {
            throw error
        }
    }
}
