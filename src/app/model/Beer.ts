import { User } from './User'

export type Beer = {
    id?: number
    image: string
    name: string
    description: string
    volume: number
    ingredients: string
    brewers_tips: string
    owner?: User
}
