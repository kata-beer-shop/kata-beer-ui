import Image from 'next/image'
import Link from 'next/link'
import { Beer } from '@/app/model/Beer'
import { BeerApi } from '@/app/api/beer-api'

export default async function BeerList() {
    const beers: Beer[] = await BeerApi.getAllBeers()

    return (
        <div className="bg-white">
            <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
                <h2 className="text-2xl font-bold tracking-tight text-gray-900 text-center">Beers</h2>
                <div className="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:gap-x-8">
                    {beers.map((beer) => (
                        <div key={beer.id} className="group relative">
                            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                                <Image
                                    src={beer.image}
                                    alt={beer.name}
                                    width={500}
                                    height={300}
                                    className="h-full w-full object-cover object-center lg:h-full lg:w-full"
                                />
                            </div>
                            <div className="mt-4 flex justify-between">
                                <div>
                                    <h3 className="text-sm text-gray-900">
                                        <Link href={`/beer-detail/${beer.name}`}>
                                            <span aria-hidden="true" className="absolute inset-0" />
                                            {beer.name}
                                        </Link>
                                    </h3>
                                </div>
                                <p className="text-sm font-medium text-gray-900">{beer.volume}L</p>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}
